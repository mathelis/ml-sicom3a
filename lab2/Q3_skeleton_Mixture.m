close all
clear all

%% Question 3.1. Displaying bivariate mixture data
nom_fich_in= 'mixture_train.mat' ;
load(nom_fich_in) ; % import dataset into variables Xtrain (and Ytrain for class labels) 

K=2 ; % binary clustering

p= 2 ; %bivariate data

% Get the sample size N (here data are bivariate, i.e. p=2)
N= length( Xtrain(:,1 ) )

% Display data points
figure
hold on
plot( Xtrain(:,1) , Xtrain(:,2),' ok' ) ;

%% TODO Question 3.2 EM clustering

%% TODO Question 3.3 Confusion and missclassification rate for EM clustering

%% Question 3.4. 

figure,
hold on
plot( Xtrain(idxperm==1,1) , Xtrain(idxperm==1,2),' or' ) ;
plot( Xtrain(idxperm==2,1) , Xtrain(idxperm==2,2),' +g' ) ;
plot( Xtrain(idxperm==3,1) , Xtrain(idxperm==3,2),' xb' ) ;

%FIXME: replace XXXX by the GMM object that fit the dataset (see Question 3.2)
ezcontour(@(x,y) pdf( XXXX ,[x y] ), [-5, 4, -2, 5],256) ; % FIXME
